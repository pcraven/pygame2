import pygame2
import pygame2.sdl
import pygame2.sdl.video
import pygame2.sdl.render
import pygamewrapper

current_window = None

def fix_color(color):
	if len(color) == 3:
		return (color[0],color[1],color[2],255)
	else:
		return color
		
class Window:
	window = None
	renderer = None
	texture = None
	
	def __init__(self, x, y, width, height ):
		self.window =  pygame2.sdl.video.create_window( "", x, y, width, height, pygame2.sdl.video.SDL_WINDOW_OPENGL  )
		self.renderer = pygame2.sdl.render.create_renderer(self.window, -1, 0)
		
	def fill(self, color):
		c=fix_color(color)
		pygame2.sdl.render.set_render_draw_color(self.renderer, c[0], c[1], c[2], c[3] )
		pygame2.sdl.render.render_clear(self.renderer)
		
	def blit(self, image, location):
		source_rect = pygame2.sdl.surface.SDL_Rect()
		
		source_rect.x=0
		source_rect.y=0
		source_rect.w=image.width
		source_rect.h=image.height
		dest_rect = pygame2.sdl.surface.SDL_Rect()
		dest_rect.x=location[0]
		dest_rect.y=location[1]
		dest_rect.w=image.width
		dest_rect.h=image.height
		if image.texture == None:
			image.texture = pygame2.sdl.render.create_texture_from_surface(pygamewrapper.display.current_window.renderer,image.surface)
			
		pygame2.sdl.render.render_copy(current_window.renderer,image.texture, None, dest_rect)
		
		

def get_current_window():
	return current_window
	
def set_mode(size):
	global current_window
	current_window = Window ( 100, 100, size[0], size[1] )
	return current_window

def set_caption(title):
	global current_window
	pygame2.sdl.video.set_window_title( current_window.window, title)
	
def flip():
	pygame2.sdl.render.render_present(current_window.renderer);