#include "pysdl.h"

static PyMethodDef sdl_methods[] = {
    {"gl_enable_smooth", gl_enable_smooth, METH_VARARGS, "gl_enable_smooth() doc string"},
    {"gl_disable_smooth", gl_disable_smooth, METH_VARARGS, "gl_disable_smooth() doc string"},
    
    {"gl_draw_arc", gl_draw_arc, METH_VARARGS, "gl_draw_arc() doc string"},
    {"gl_draw_ellipse", gl_draw_ellipse, METH_VARARGS, "gl_draw_ellipse() doc string"},
    {"gl_draw_line", gl_draw_line, METH_VARARGS, "gl_draw_line() doc string"},
    {"gl_draw_lines", gl_draw_lines, METH_VARARGS, "gl_draw_lines() doc string"},
    {"gl_draw_polygon", gl_draw_polygon, METH_VARARGS, "gl_draw_polygon() doc string"},
    {"gl_draw_rect", gl_draw_rect, METH_VARARGS, "gl_draw_rect() doc string"},

    {NULL, NULL}
};

static struct PyModuleDef sdlmodule = {
    PyModuleDef_HEAD_INIT,
    "pygameaux",
    "pygameaux module doc string",
    -1,
    sdl_methods,
    NULL,
    NULL,
    NULL,
    NULL
};

PyMODINIT_FUNC
PyInit_pygameaux(void)
{
	PyObject* m;

    m = PyModule_Create(&sdlmodule);
    if (m == NULL)
        return NULL;

    return m;
}