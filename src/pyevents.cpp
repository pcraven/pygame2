#include "pysdl.h"

PyObject *
poll_event(PyObject *self, PyObject *args)
{
	SDL_Event event;
	if(SDL_PollEvent(&event)){
		PyEventObject* pyEventObject;
		pyEventObject = PyObject_New(PyEventObject, &PyEventType);
		pyEventObject->type = 0;
		switch(event.type)
			case SDL_QUIT:
				pyEventObject->type = 1;
		return (PyObject*)pyEventObject;
	} else {
		Py_INCREF(Py_None);
		return Py_None;
	}
}
