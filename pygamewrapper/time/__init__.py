import pygame2.sdl.timer
import time

def get_ticks():
	return pygame2.sdl.timer.get_ticks()

class Clock():
	last_time = 0
	
	def tick(self, fps):
		new_time = time.time()
		if self.last_time > 0:
			elapsed_time = new_time - self.last_time
			wait_time = 1/fps - elapsed_time
			if wait_time > 0:
				time.sleep(wait_time)
		self.last_time = new_time
			