import pygame2.sdl
import pygamewrapper.display
import pygame2
import pygame2.sdlimage
import pygamewrapper

class Image():
	surface = None
	texture = None
	width = 0
	height = 0
	
	def convert(self):
		return self
		
	def set_colorkey(self, color):
		int_color = pygamewrapper.get_int_color(color)
		pygame2.sdl.surface.set_color_key(self.surface, pygame2.sdl.SDL_TRUE, int_color)
		self.texture = pygame2.sdl.render.create_texture_from_surface(pygamewrapper.display.current_window.renderer,self.surface)

def load(filename):
	image = Image()
	
	image.surface = pygame2.sdlimage.load(filename)
	image.width = image.surface._w
	image.height = image.surface._h
	image.texture = pygame2.sdl.render.create_texture_from_surface(pygamewrapper.display.current_window.renderer,image.surface)
	print("Load image")
	return image

	