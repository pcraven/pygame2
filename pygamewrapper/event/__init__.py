import pygame2.sdl.events

def get():
	event_list = []
	event = pygame2.sdl.events.poll_event(True)
	while event != None:
		event_list.append(event)
		event = pygame2.sdl.events.poll_event(True)
	return event_list