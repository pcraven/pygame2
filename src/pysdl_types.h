typedef struct {
    PyObject_HEAD
    SDL_Window* window;
} PyWindowObject;

extern PyTypeObject PyWindowType;

typedef struct {
    PyObject_HEAD
    SDL_Renderer* renderer;
} PyRendererObject;

extern PyTypeObject PyRendererType;

typedef struct {
    PyObject_HEAD
    int type;
} PyEventObject;

extern PyTypeObject PyEventType;

typedef struct {
    PyObject_HEAD
    SDL_Texture* texture;
} PyTextureObject;

extern PyTypeObject PyTextureType;