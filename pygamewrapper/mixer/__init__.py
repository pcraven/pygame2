import pygame2.audio

sink = pygame2.audio.SoundSink()
source = pygame2.audio.SoundSource()

class Sound():
    sound = None
    
    def __init__(self, filename):
        self.sound = pygame2.audio.load_file(filename)
        
    def play(self):
        source.queue(self.sound)
        source.request = pygame2.audio.SOURCE_PLAY
        sink.process_source(source)