import pygamewrapper.display
import pygamewrapper.time
import pygamewrapper.event
import pygamewrapper.draw
import pygamewrapper.image
import pygamewrapper.mouse
import pygamewrapper.mixer
import pygamewrapper.sprite
import pygamewrapper.joystick
import pygamewrapper.rect

import pygame2.sdl
import pygame2.sdl.hints
import pygame2.sdl.events
import pygame2.sdl.rect
import pygame2.sdl.surface

QUIT = pygame2.sdl.events.SDL_QUIT
MOUSEBUTTONDOWN = pygame2.sdl.events.SDL_MOUSEBUTTONDOWN
KEYDOWN = pygame2.sdl.events.SDL_KEYDOWN
KEYUP = pygame2.sdl.events.SDL_KEYUP

def init():
	pygame2.sdl.hints.set_hint(pygame2.sdl.hints.SDL_HINT_RENDER_DRIVER,"opengl");
	pygame2.sdl.init()
	print("pygame wrapper init")

def quit():
	pygame2.sdl.quit()

def get_int_color( color ):
	return (color[0] << 16) + (color[1] << 8) + color[0]
	
class Rect():
	x = 0
	y = 0
	w = 0
	h = 0
	
	def __getitem__(self,key):
		if key == 0:
			return self.x
		if key == 1:
			return self.y
		if key == 2:
			return self.w
		if key == 3:
			return self.w
		raise IndexError()
		
	def __init__(self, x, y, w, h):
		self.x = x
		self.y = y
		self.w = w
		self.h = h
	
	def colliderect(self, r2):
		if self.x < r2.x+r2.w and self.x+self.w > r2.x and self.y < r2.y+r2.h and self.y+self.h > r2.y:
			return True
						
class Surface():
	surface = None
	texture = None
	rect = None
	width = 0
	height = 0
	
	def __init__(self, size):
		self.width = size[0]
		self.height = size[1]
		self.surface = pygame2.sdl.surface.create_rgb_surface(self.width, self.height, 8)
		self.rect = Rect(0,0,self.width,self.height)

		
	def fill(self, color):
		rect = pygame2.sdl.rect.SDL_Rect()
		rect.x = 0
		rect.y = 0
		rect.h = self.rect.h
		rect.w = self.rect.w
		pygame2.sdl.surface.fill_rect(self.surface, rect, pygamewrapper.get_int_color(color) )
		# self.texture = something needs to go here.
	
	def get_rect(self):
		return self.rect