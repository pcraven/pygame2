PyObject *
render_draw_line(PyObject *self, PyObject *args);

PyObject *
gl_render_draw_line(PyObject *self, PyObject *args);

PyObject *
gl_draw_line(PyObject *self, PyObject *args);

PyObject* 
gl_draw_lines(PyObject* self, PyObject* arg);

PyObject *
gl_enable_smooth(PyObject *self, PyObject *args);

PyObject *
gl_disable_smooth(PyObject *self, PyObject *args);

PyObject *
gl_draw_rect(PyObject *self, PyObject *args);

PyObject *
gl_draw_ellipse(PyObject *self, PyObject *args);

PyObject *
gl_draw_arc(PyObject *self, PyObject *args);

PyObject *
set_render_draw_color(PyObject *self, PyObject *args);

PyObject *
render_present(PyObject *self, PyObject *args);

PyObject *
render_clear(PyObject *self, PyObject *args);

PyObject *
render_draw_rect(PyObject *self, PyObject *args);

PyObject *
render_fill_rect(PyObject *self, PyObject *args);

PyObject *
blend_mode(PyObject *self, PyObject *args);

PyObject *
gl_draw_polygon(PyObject *self, PyObject *args);

PyObject *
load_texture(PyObject *self, PyObject *args);

PyObject *
render_copy(PyObject *self, PyObject *args);

