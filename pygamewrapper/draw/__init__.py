from pygameaux import *
from pygamewrapper.display import *
import math

def rect(window, color, rect, width=0):
	gl_draw_rect(fix_color(color), rect, width);

def ellipse(window, color, rect, width=0):
	gl_draw_ellipse(fix_color(color), rect, width);

def arc(window, color, rect, startarc, endarc, width=0):
	gl_draw_arc(fix_color(color), rect, startarc, endarc, width);

def circle(window, color, center, radius, width=0):
	rect = (center[0] - radius, center[1] - radius, radius*2, radius*2)
	gl_draw_ellipse(fix_color(color), rect, width);

def line(window, color, startpoint, endpoint, width=1):
	gl_draw_line(fix_color(color), startpoint, endpoint, width);

def aaline(window, color, startpoint, endpoint, width=1):
	gl_enable_smooth();
	gl_draw_line(fix_color(color), startpoint, endpoint, width);
	gl_disable_smooth();

def lines(window, color, closed, points, width):
	gl_draw_lines(fix_color(color),closed, points, width);

def polygon(window, color, points, width=1):
	gl_draw_polygon(fix_color(color), points, width);

		