#include "pysdl.h"
#include "structmember.h"

PyTypeObject PyWindowType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "sdl.PyWindow",             /* tp_name */
    sizeof(PyWindowObject), /* tp_basicsize */
    0,                         /* tp_itemsize */
    0,                         /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_reserved */
    0,                         /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash  */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,        /* tp_flags */
    "SDL Window objects",           /* tp_doc */
};

PyTypeObject PyRendererType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "sdl.PyRenderer",             /* tp_name */
    sizeof(PyRendererObject), /* tp_basicsize */
    0,                         /* tp_itemsize */
    0,                         /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_reserved */
    0,                         /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash  */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,        /* tp_flags */
    "SDL Renderer objects",           /* tp_doc */
};

static PyObject *
event_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    PyEventObject *self;

    self = (PyEventObject *)type->tp_alloc(type, 0);
    if (self != NULL) {
        self->type = 0;
    }

    return (PyObject *)self;
}

static int
event_init(PyEventObject *self, PyObject *args, PyObject *kwds)
{
    static char *kwlist[] = {"type", NULL};

    if (! PyArg_ParseTupleAndKeywords(args, kwds, "|i", kwlist,
                                      &self->type))
        return -1;

    return 0;
}

static PyMemberDef event_members[] = {
    {"type", T_INT, offsetof(PyEventObject, type), 0,
     "type number"},
    {NULL}  /* Sentinel */
};

PyTypeObject PyEventType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "sdl.PyEvent",             /* tp_name */
    sizeof(PyEventObject),     /* tp_basicsize */
    0,                         /* tp_itemsize */
    0,                         /* tp_dealloc */
    0,                         /* tp_print */
    0,             /* tp_getattr */
    0,             /* tp_setattr */
    0,                         /* tp_reserved */
    0,                         /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash  */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,        /* tp_flags */
    "SDL Event objects",       /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    0,             /* tp_methods */
    event_members,             /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)event_init,      /* tp_init */
    0,                         /* tp_alloc */
    event_new,                 /* tp_new */	
};

PyTypeObject PyTextureType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "sdl.PyTexture",             /* tp_name */
    sizeof(PyTextureObject), /* tp_basicsize */
    0,                         /* tp_itemsize */
    0,                         /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_reserved */
    0,                         /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash  */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,        /* tp_flags */
    "SDL Texture objects",           /* tp_doc */
};
 
PyObject *
quit(PyObject *self, PyObject *args)
{
    SDL_Quit();
	
	Py_INCREF(Py_None);
	return Py_None;
}



