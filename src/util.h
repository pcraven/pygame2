int
IntFromObj (PyObject* obj, int* val);

int
IntFromObjIndex (PyObject* obj, int _index, int* val);

int
TwoIntsFromObj (PyObject* obj, int* val1, int* val2);
