PyObject *
create_window(PyObject *self, PyObject *args);

PyObject *
init(PyObject *self, PyObject *args);

PyObject *
create_renderer(PyObject *self, PyObject *args);

PyObject *
destroy_window(PyObject *self, PyObject *args);

PyObject *
set_window_title(PyObject *self, PyObject *args);