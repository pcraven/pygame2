#include "pysdl.h"
PyObject *
init(PyObject *self, PyObject *args)
{
	SDL_SetHint(SDL_HINT_RENDER_DRIVER,"opengl");
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        printf("SDL_Init failed: %s\n", SDL_GetError());
	}
	Py_INCREF(Py_None);
	return Py_None;

}
PyObject *
create_window(PyObject *self, PyObject *args)
{
    int w = 0;
    int h = 0;
	int x = 0;
	int y = 0;
    if (!PyArg_ParseTuple (args, "iiii", &x, &y, &w, &h))
        return NULL;
		
    SDL_Window *window;
    window = SDL_CreateWindow("Python Window", x, y, w, h, SDL_WINDOW_SHOWN);
    if (window == nullptr){
        printf( SDL_GetError() );
		
        Py_INCREF(Py_None);
        return Py_None;
    }
    PyWindowObject* winObj;
    winObj = PyObject_New(PyWindowObject, &PyWindowType);
    winObj->window = window;
    return (PyObject*)winObj;
}

PyObject *
create_renderer(PyObject *self, PyObject *args)
{
    PyWindowObject *pyWindowObject;
	if(!PyArg_ParseTuple(args, "O!", &PyWindowType, &pyWindowObject))
		return NULL;

    SDL_Renderer *renderer = SDL_CreateRenderer(pyWindowObject->window, -1, SDL_RENDERER_ACCELERATED);

    PyRendererObject* rendererObj;
    rendererObj = PyObject_New(PyRendererObject, &PyRendererType);
    rendererObj->renderer = renderer;

    SDL_RendererInfo info;
    SDL_GetRendererInfo( rendererObj->renderer, &info );

    if( info.flags | SDL_RENDERER_ACCELERATED)
        printf("Accelerated\r\n");
    else if( info.flags | SDL_RENDERER_SOFTWARE)
        printf("Software\r\n");
    else
        printf("Unknown\r\n");

    const char *error = SDL_GetError();
    if( error && strlen(error) > 0)
        printf("Renderer failed: %s\n", error);

    return (PyObject*)rendererObj;
}

PyObject *
destroy_window(PyObject *self, PyObject *args)
{
	PyWindowObject *pyWindowObject;

	if(!PyArg_ParseTuple(args, "O!", &PyWindowType, &pyWindowObject))
		return Py_None;
    
    SDL_DestroyWindow(pyWindowObject->window);
	
	Py_INCREF(Py_None);
	return Py_None;
}

PyObject *
set_window_title(PyObject *self, PyObject *args)
{
	PyWindowObject *pyWindowObject;
	char *title;
	if(!PyArg_ParseTuple(args, "O!s", &PyWindowType, &pyWindowObject, &title)) {
		Py_INCREF(Py_None);
		return Py_None;
	}
    
    SDL_SetWindowTitle(pyWindowObject->window, title);
	
	Py_INCREF(Py_None);
	return Py_None;
}


