#ifndef _PYSDL_H_
#define _PYSDL_H

#include <stdio.h>

#include "Python.h"
#include "SDL.h"

#include "pysdl_types.h"
#include "pyrenderer.h"
#include "pysdlvideo.h"
#include "pyevents.h"
#include "util.h"

#define RAISE(x,y) (PyErr_SetString((x), (y)), (PyObject*)NULL)

#ifndef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a,b) ( (a) > (b) ? (a) : (b))
#endif

#ifndef ABS
#define ABS(a) (((a) < 0) ? -(a) : (a))
#endif

PyObject *
create_window(PyObject *self, PyObject *args);

PyObject *
create_renderer(PyObject *self, PyObject *args);

PyObject *
destroy_window(PyObject *self, PyObject *args);

PyObject *
quit(PyObject *self, PyObject *args);


#endif
