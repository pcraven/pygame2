#include "pysdl.h"
#include "SDL_opengl.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")


PyObject *
gl_draw_line(PyObject *self, PyObject *args)
{
    int r, g, b, a, x1, y1, y2, x2, width;
    
	if(!PyArg_ParseTuple(args, "(iiii)(ii)(ii)i", &r, &g, &b, &a, &x1, &y1, &x2, &y2, &width)) {
		Py_INCREF(Py_None);
		return Py_None;
	}
		
	glColor4ub( r,g,b,a );
	glLineWidth( float(width) ); 
	glBegin(GL_LINES);
	glVertex2i(x1, y1);
	glVertex2i(x2, y2);
        glColor4ub( 255, 255, 255, 255 );
	glEnd();
	glLineWidth( 1.0f ); 
	
	Py_INCREF(Py_None);
    return Py_None;
}

PyObject *
gl_enable_smooth(PyObject *self, PyObject *args)
{
	glEnable (GL_LINE_SMOOTH);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint (GL_LINE_SMOOTH_HINT, GL_DONT_CARE); 
    
	Py_INCREF(Py_None);
	return Py_None;
    
}

PyObject *
gl_disable_smooth(PyObject *self, PyObject *args)
{
	glDisable (GL_LINE_SMOOTH);
	glDisable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint (GL_LINE_SMOOTH_HINT, GL_DONT_CARE); 
    
	Py_INCREF(Py_None);
	return Py_None;
    
}

PyObject *
gl_draw_rect(PyObject *self, PyObject *args)
{
	int r, g, b, a, x, y, w, h, width;
    
	if(!PyArg_ParseTuple(args, "(iiii)(iiii)i", &r, &g, &b, &a, &x, &y, &w, &h, &width)) {
		Py_INCREF(Py_None);
		return Py_None;
	}
	
	int x1=x;
	int y1=y;
	int x2=x+w;
	int y2=y+h;
	
	glColor4ub( r,g,b,a );
	glLineWidth( float(width) );
	if( width > 0 )
		glBegin(GL_LINE_LOOP);
	else
		glBegin(GL_QUADS);
	glVertex2i(x1, y1);
	glVertex2i(x2, y1);
	glVertex2i(x2, y2);
	glVertex2i(x1, y2);
        glColor4ub( 255, 255, 255, 255 );
	glEnd();
	glLineWidth( 1.0f ); 
	
	Py_INCREF(Py_None);
    return Py_None;
}

PyObject *
gl_draw_ellipse(PyObject *self, PyObject *args)
{
    int r, g, b, a, x, y, w, h, width;
    
	if(!PyArg_ParseTuple(args, "(iiii)(iiii)i", &r, &g, &b, &a, &x, &y, &w, &h, &width)) {
		Py_INCREF(Py_None);
		return Py_None;
	}
	
	glColor4ub( r,g,b,a );

	double x2,y2;
	double t;
	
	if( width == 0 ) {
		glBegin(GL_TRIANGLE_FAN);
	} else {
		glLineWidth( float(width) );
		glBegin(GL_LINE_STRIP);
	}
	
	double cx=x+w/2;
	double cy=y+h/2;
	double hh=h/2;
	double hw=w/2;
	for(t = 0; t <= 6.29; t +=0.05)
	{
		x2 = hw*sin(t)+cx;
		y2 = hh*cos(t)+cy;
		glVertex2f((float)x2,(float)y2);
	}
        glColor4ub( 255, 255, 255, 255 );
	glEnd();
	Py_INCREF(Py_None);
    return Py_None;
}

// draw.arc(screen,black,[210,75,150,125], 0, pi/2, 2)
PyObject *
gl_draw_arc(PyObject *self, PyObject *args)
{
    int r, g, b, a, x, y, w, h, width;
    double startarc, endarc;
	
	
	if(!PyArg_ParseTuple(args, "(iiii)(iiii)ddi", &r, &g, &b, &a, &x, &y, &w, &h, &startarc, &endarc, &width)) {
		Py_INCREF(Py_None);
		return Py_None;
	}
	
	glColor4ub( r,g,b,a );

	double x2,y2;
	double t;
	double cx=x+w/2;
	double cy=y+h/2;
	double hh=h/2;
	double hw=w/2;

	if( width == 0 ) {
		glBegin(GL_TRIANGLE_FAN);
	} else {
		glLineWidth( float(width) );
		glBegin(GL_LINE_STRIP);
	}
	for(t = startarc; t <= endarc; t +=.05)
	{
		x2 = hw*sin(t)+cx;
		y2 = hh*cos(t)+cy;
		glVertex2f((float)x2,(float)y2);
	}
        glColor4ub( 255, 255, 255, 255 );
	glEnd();
	
	Py_INCREF(Py_None);
    return Py_None;
}

PyObject* 
gl_draw_lines(PyObject* self, PyObject* arg)
{
	PyObject *closedobj, *points, *item;
	int r,g, b, a;
	
	int x, y;
	int top, left, bottom, right;
	int pts[4], width=1;
	int closed;
	int result, loop, length;
	int startx, starty;

	/*get all the arguments*/
	if(!PyArg_ParseTuple(arg, "(iiii)OO|i", &r,&g,&b,&a, &closedobj, &points, &width))
		return NULL;

	closed = PyObject_IsTrue(closedobj);

	if(!PySequence_Check(points))
		return RAISE(PyExc_TypeError, "points argument must be a sequence of number pairs");
	length = PySequence_Length(points);
	if(length < 2)
		return RAISE(PyExc_ValueError, "points argument must contain more than 1 points");

	item = PySequence_GetItem(points, 0);
	result = TwoIntsFromObj(item, &x, &y);
	Py_DECREF(item);
	if(!result) return RAISE(PyExc_TypeError, "points must be number pairs");

	startx = pts[0] = left = right = x;
	starty = pts[1] = top = bottom = y;

	glLineWidth( float(width) );
	glColor4ub( r,g,b,a );
	glBegin(GL_LINE_STRIP);
	for(loop = 0; loop < length; ++loop)
	{
		item = PySequence_GetItem(points, loop);
		result = TwoIntsFromObj(item, &x, &y);
		Py_DECREF(item);
		if(!result) continue; /*note, we silently skip over bad points :[ */
		glVertex2i(x, y);

	}
        glColor4ub( 255, 255, 255, 255 );
	glEnd();

    Py_INCREF(Py_None);
    return Py_None;

}

PyObject* 
gl_draw_polygon(PyObject* self, PyObject* arg)
{
	PyObject *points, *item;
	int r,g, b, a;
	
	int x, y;
	int top, left, bottom, right;
	int pts[4], width=1;
	int result, loop, length;
	int startx, starty;

	/*get all the arguments*/
	if(!PyArg_ParseTuple(arg, "(iiii)O|i", &r,&g,&b,&a, &points, &width))
		return NULL;

	if(!PySequence_Check(points))
		return RAISE(PyExc_TypeError, "points argument must be a sequence of number pairs");
	length = PySequence_Length(points);
	if(length < 2)
		return RAISE(PyExc_ValueError, "points argument must contain more than 1 points");

	item = PySequence_GetItem(points, 0);
	result = TwoIntsFromObj(item, &x, &y);
	Py_DECREF(item);
	if(!result) return RAISE(PyExc_TypeError, "points must be number pairs");

	startx = pts[0] = left = right = x;
	starty = pts[1] = top = bottom = y;

	glLineWidth( float(width) );
	glColor4ub( r,g,b,a );
	glBegin(GL_LINE_LOOP);
	for(loop = 0; loop < length; ++loop)
	{
		item = PySequence_GetItem(points, loop);
		result = TwoIntsFromObj(item, &x, &y);
		Py_DECREF(item);
		if(!result) continue; /*note, we silently skip over bad points :[ */
		glVertex2i(x, y);

	}
        glColor4ub( 255, 255, 255, 255 );
	glEnd();

    Py_INCREF(Py_None);
    return Py_None;

}
